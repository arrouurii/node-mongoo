const express = require('express');
const app = express();
const connectToMongodb = require('./settings/database/database-connection');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv/config');

// Middlewares
app.use(bodyParser.json());
app.use(cors());
const postsRoute = require('./routes/posts');
const usersRoute = require('./routes/users');

app.use('/posts', postsRoute);
app.use('/users', usersRoute);

// Routes
app.get('/health', (req, res) => {
  res.status(200).send('UP');
});

connectToMongodb();

app.listen(process.env.PORT);
