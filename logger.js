const byteToGigabyte = (bytes) => {
    return Math.round(bytes / Math.pow(1024, 3));
}

module.exports = byteToGigabyte;