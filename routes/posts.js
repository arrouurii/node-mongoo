const express = require('express');
const router = express.Router();
const Post = require('../models/Post');

router.get('/', async (req, res) => {
  try {
    const posts = await Post.find();

    res.send(posts);
  } catch (error) {
    res.json({ message: error });
  }
});

router.post('/', async (req, res) => {
  const post = new Post({
    ...req.body,
  });

  try {
    const savedPost = await post.save();

    res.status(201).json(savedPost);
  } catch (error) {
    res.status(500);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const post = await Post.findById(id);
    res.json(post);
  } catch (error) {
    res.status(500);
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const post = await Post.remove({ _id: id });
    res.json(post);
  } catch (error) {
    res.status(500);
  }
});

router.patch('/:id', async (req, res) => {
  try {
    const { body: post } = req;
    const { id } = req.params;
    const updatedPost = await Post.updateOne(
      { _id: id },
      { $set: { ...post } }
    );
    res.json(updatedPost);
  } catch (error) {
    res.status(500);
  }
});

module.exports = router;
