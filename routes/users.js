const express = require('express');
const User = require('../models/User');
const router = express.Router();

router.post('/', async (req, res) => {
    const user = new User(req.body);

    try {
        const savedUser = await user.save();

        res.status(201).send(savedUser);
    } catch (error) {
        res.send({message: error});
    }
});

router.get('/', async (req, res) => {
    try {
        const users = await User.find();

        res.status(200).send(users);
    } catch (error) {
        res.send({message: error});
    }
});

module.exports = router;