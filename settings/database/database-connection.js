const connectToMongodb = () => {
  const mongoose = require('mongoose');
  const uri = process.env.DB_CONNECTION;

  const connectionOptions = {
    auth: { authSource: 'admin' },
    user: process.env.DB_USER,
    pass: process.env.DB_PWD,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  };

  mongoose.connect(
    uri,
    connectionOptions,
    (error) => {
      if (error) {
        console.error(error);

        return;
      }

      console.log('DB :: connected succefully !');
    }
  );
};

module.exports = connectToMongodb;
