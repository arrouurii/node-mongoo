const os = require('os');
const byteToGigabyte = require('./logger');
const fs = require('fs');
const http = require('http');

const server = http.createServer((req, res) => {
    console.log(req.url);
    
    if ( req.url === '/' ) {
        res.write('Hello world');
        res.end();
    }
});

server.listen(4500);

console.log('listening on port 4500');
// console.log('Total memory : ' + byteToGigabyte(os.totalmem()) + ' GB');
// console.log('Free memory : ' + byteToGigabyte(os.freemem()) + ' GB');
